import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-content-slider',
  templateUrl: './content-slider.component.html',
  styleUrls: ['./content-slider.component.css']
})
export class ContentSliderComponent implements OnInit {

  data = [
    {
      title: 'Title-A',
      imagePath: '/assets/images/image-1.jpg',
      content: `A The Taj Mahal (/ˌtɑːdʒ məˈhɑːl, ˌtɑːʒ-/;[4] Hindi: ताज महल [taːdʒ ˈmɛːɦ(ə)l], meaning "Crown of the Palaces")[5] is an ivory-white marble mausoleum on the south bank of the Yamuna river in the Indian city of Agra. It was commissioned in 1632 by
      the Mughal emperor, Shah Jahan (reigned from 1628 to 1658), to house the tomb of his favourite wife, Mumtaz Mahal. It also houses the tomb of Shah Jahan, the builder. The tomb is the centerpiece of a 17-hectare (42-acre) complex, which includes
      a mosque and a guest house, and is set in formal gardens bounded on three sides by a crenellated wall. Construction of the mausoleum was essentially completed in 1643 but work continued on other phases of the project for another 10 years.
      The Taj Mahal complex is believed to have been completed in its entirety in 1653 at a cost estimated at the time to be around 32 million rupees, which in 2015 would be approximately 52.8 billion rupees (U.S. $827 million). The construction
      project employed some 20,000 artisans under the guidance of a board of architects led by the court architect to the emperor, Ustad Ahmad Lahauri. The Taj Mahal was designated as a UNESCO World Heritage Site in 1983 for being "the jewel of
      Muslim art in India and one of the universally admired masterpieces of the world's heritage". It is regarded by many as the best example of Mughal architecture and a symbol of India's rich history. The Taj Mahal attracts 7–8 million visitors
      a year and in 2007, it was declared a winner of the New7Wonders of the World (2000–2007) initiative.`,
      grey: false
    },
    {
      title: 'Title-B',
      imagePath: '/assets/images/image-2.jpg',
      content: `B The Taj Mahal (/ˌtɑːdʒ məˈhɑːl, ˌtɑːʒ-/;[4] Hindi: ताज महल [taːdʒ ˈmɛːɦ(ə)l], meaning "Crown of the Palaces")[5] is an ivory-white marble mausoleum on the south bank of the Yamuna river in the Indian city of Agra. It was commissioned in 1632 by
      the Mughal emperor, Shah Jahan (reigned from 1628 to 1658), to house the tomb of his favourite wife, Mumtaz Mahal. It also houses the tomb of Shah Jahan, the builder. The tomb is the centerpiece of a 17-hectare (42-acre) complex, which includes
      a mosque and a guest house, and is set in formal gardens bounded on three sides by a crenellated wall. Construction of the mausoleum was essentially completed in 1643 but work continued on other phases of the project for another 10 years.
      The Taj Mahal complex is believed to have been completed in its entirety in 1653 at a cost estimated at the time to be around 32 million rupees, which in 2015 would be approximately 52.8 billion rupees (U.S. $827 million). The construction
      project employed some 20,000 artisans under the guidance of a board of architects led by the court architect to the emperor, Ustad Ahmad Lahauri. The Taj Mahal was designated as a UNESCO World Heritage Site in 1983 for being "the jewel of
      Muslim art in India and one of the universally admired masterpieces of the world's heritage". It is regarded by many as the best example of Mughal architecture and a symbol of India's rich history. The Taj Mahal attracts 7–8 million visitors
      a year and in 2007, it was declared a winner of the New7Wonders of the World (2000–2007) initiative.`,
      grey: false
    },
    {
      title: 'Title-C',
      imagePath: '/assets/images/image-3.jpg',
      content: `C The Taj Mahal (/ˌtɑːdʒ məˈhɑːl, ˌtɑːʒ-/;[4] Hindi: ताज महल [taːdʒ ˈmɛːɦ(ə)l], meaning "Crown of the Palaces")[5] is an ivory-white marble mausoleum on the south bank of the Yamuna river in the Indian city of Agra. It was commissioned in 1632 by
      the Mughal emperor, Shah Jahan (reigned from 1628 to 1658), to house the tomb of his favourite wife, Mumtaz Mahal. It also houses the tomb of Shah Jahan, the builder. The tomb is the centerpiece of a 17-hectare (42-acre) complex, which includes
      a mosque and a guest house, and is set in formal gardens bounded on three sides by a crenellated wall. Construction of the mausoleum was essentially completed in 1643 but work continued on other phases of the project for another 10 years.
      The Taj Mahal complex is believed to have been completed in its entirety in 1653 at a cost estimated at the time to be around 32 million rupees, which in 2015 would be approximately 52.8 billion rupees (U.S. $827 million). The construction
      project employed some 20,000 artisans under the guidance of a board of architects led by the court architect to the emperor, Ustad Ahmad Lahauri. The Taj Mahal was designated as a UNESCO World Heritage Site in 1983 for being "the jewel of
      Muslim art in India and one of the universally admired masterpieces of the world's heritage". It is regarded by many as the best example of Mughal architecture and a symbol of India's rich history. The Taj Mahal attracts 7–8 million visitors
      a year and in 2007, it was declared a winner of the New7Wonders of the World (2000–2007) initiative.`,
      grey: false
    }

  ];


  title = '';
  imagePath = '';
  content = '';


  constructor() {
    console.log('i am constructor');
  }

  ngOnInit() {
    console.log('i am ngOnInit');
    this.title = this.data[0].title;
    this.imagePath = this.data[0].imagePath;
    this.content = this.data[0].content;
    this.data[0].grey = true;
  }

  updateAllContent(page) {
    console.log(page);
    console.log(this.data[page]);
    this.title = this.data[page].title;
    this.imagePath = this.data[page].imagePath;
    this.content = this.data[page].content;
    this.data.map((elm) => {
      elm.grey = false;
    });
    this.data[page].grey = true;

  }


}
